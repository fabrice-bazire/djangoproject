# DjangoProject

## But du projet 

Le but du projet consiste à gérer une collection de Film, Série et Réalisateur.
Nous avons une base de donnée gérer avec sqlite. 

## Base de donnée

La base de donnée contient plusieurs tables. 

| Film        | Realisateur | Serie       |
|-------------|-------------|-------------|
| id          | id          | id          |
| nom         | nom         | nom         |
| description | prenom      | description |
| sortie      | nationalite | sortie      |
| img_id      | alive       | nb_saison   |
|             | img_id      | img_id      |

Un réalisateur est relié par une relation many to many à 
Serie et Film.

Ces trois tables contiennent une relation à la table image.

| Image     |
|-----------|
| id        |
| name      |
| imageFile |

[//]: <> (Explications Fabrice du fonctionnement d'image)

## Lancer le projet (Application)

Pour lancer l'application projectMovie il faut s'assurer d'avoir django d'installer. 
Plusieurs solutions s'offre à vous, généralement on utilise un virtualenv (https://docs.python.org/3/tutorial/venv.html).
Pour lancer le virtualenv vous devez avoir python instalé sur votre systeme (https://www.python.org/downloads/).

Ouvrez un terminal et placez-vous dans le dossier *./projectMovie*. 
Une fois dans dossier: 
* **activer le virtualenv**
* `./manage.py migrate` -> créer et met à jour la bd grace aux fichiers du dossier *./projectMovie/movies/migrations*
(pour voir les migrations `./manage.py showmigrations`)
* `./manage.py runserver` -> lance un serveur local


Un lien s'affiche dans votre terminal http://127.0.0.1:8000/, cliquez sur le lien. 

### Page not found
||
|----------------------|
| admin/               |
| movies/              |
| ^media/(?P<path>.*)$ |

Concentrons nous sur les deux premiers liens, **admin/** et **movies/**. 

#### Admin
Pour gérer la partie admin, il faut d'abord se créer un compte admin, pour ce faire:
* `./manage.py createsuperuser`

Si vous voulez changer votre mot de passe `./manage.py changepassword`

Sinon dirigérez-vous dans le dossier *http://127.0.0.1:8000/admin* et créé, modifié ou supprimé vos instances.

#### Movies

Pour aller dans l'interface utilisateur, plusieurs route s'offre à vous. Il y a des routes pour l'home,
les films, les séries et pour les réalisateurs.  

Vous trouverez les différents liens accessibles ci dessous, ou de facon plus approfondis =>  http://127.0.0.1:8000/movies/

* http://127.0.0.1:8000/movies/home

* http://127.0.0.1:8000/movies/series

* http://127.0.0.1:8000/movies/films

* http://127.0.0.1:8000/movies/realisateur

## Scrapping

Pour scrapper des données, rendez-vous dans le dossier *./scrap*
Le dossier se présente comme cela :

# scrap

* [scrap/](.\scrap\scrap)
  * [spiders/](.\scrap\scrap\spiders)
    * [__pycache__/](.\scrap\scrap\spiders\__pycache__)
      * [serie_spider.cpython-38.pyc](.\scrap\scrap\spiders\__pycache__\serie_spider.cpython-38.pyc)
      * [__init__.cpython-38.pyc](.\scrap\scrap\spiders\__pycache__\__init__.cpython-38.pyc)
    * [serie_spider.py](.\scrap\scrap\spiders\serie_spider.py)
    * [__init__.py](.\scrap\scrap\spiders\__init__.py)
  * [__pycache__/](.\scrap\scrap\__pycache__)
  * [items.py](.\scrap\scrap\items.py)
  * [middlewares.py](.\scrap\scrap\middlewares.py)
  * [pipelines.py](.\scrap\scrap\pipelines.py)
  * [settings.py](.\scrap\scrap\settings.py)
  * [__init__.py](.\scrap\scrap\__init__.py)
* [scrapy.cfg](.\scrap\scrapy.cfg)
* [__init__.py](.\scrap\__init__.py)

### Lancer avec scrapy 

Pour lancer une commande avec scrapy il faut tout d'abord l'installer (https://docs.scrapy.org/en/latest/intro/install.html). 

Une fois installé, lancer la commande :
* `scrapy crawl` <nom définit dans .\projectMovie\scrap\scrap\spiders\serie_spider.py>


 



