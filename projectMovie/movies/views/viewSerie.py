from django.shortcuts import render
from django.urls import reverse_lazy
from django.http import HttpResponse
from django.views.generic import DetailView, ListView, UpdateView, CreateView, DeleteView
from movies.models import Serie
from django.shortcuts import reverse
from django.contrib import messages

# Create your views here.

class SerieListView(ListView):
    paginate_by = 10
    model = Serie

class SerieDetailView(DetailView):
    model = Serie

class SerieDeleteView(DeleteView):
    model = Serie
    template_name_suffix = '_delete'
    
    def get_success_url(self):
        messages.success(self.request, 'Serie Supprimé')
        return reverse('movies:serie_list')

class SerieUpdateView(UpdateView):
    model = Serie
    fields = ('nom','sortie','description','nb_saison','realisateurs', 'image')
    template_name_suffix = '_edit'

    def get_success_url(self):
        messages.success(self.request, 'Serie Mise à jour')
        return reverse('movies:serie_list')

class SerieCreateView(CreateView):
    model = Serie
    fields = ('nom','sortie','description','nb_saison','realisateurs', 'image')
    template_name_suffix = '_create'

    def get_success_url(self):
        messages.success(self.request, 'Serie Mise à jour')
        return reverse('movies:serie_list')

