from django.shortcuts import render
from django.urls import reverse_lazy
from django.http import HttpResponse
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from movies.models import Film, Realisateur
from django.core.paginator import Paginator

# Create your views here.

class FilmList(ListView):
    paginate_by = 12
    model = Film

    # def listing(request):
    #     modele = Film.objects.all()
    #     paginator = Paginator(modele, 25)  # Show 25 contacts per page
    #     page = request.GET.get('page')
    #     film = paginator.get_page(page)
    #
    #     return render(request, 'movies:film_list', {'films': film})

class FilmDetailView(DetailView):
    model = Film

class FilmCreate(CreateView):
    model = Film
    fields = ['nom', 'description', 'sortie', 'realisateurs', 'img']
    success_url = reverse_lazy('movies:film_list')

class FilmUpdate(UpdateView):
    model = Film
    fields = ['nom', 'description', 'sortie', 'realisateurs', 'img']
    template_name_suffix = '_update_form'
    success_url = reverse_lazy('movies:film_list')

class FilmDelete(DeleteView):
    model = Film
    success_url = reverse_lazy('movies:film_list')

