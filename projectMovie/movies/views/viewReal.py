from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import DetailView, ListView, UpdateView, CreateView, DeleteView
from movies.models import Realisateur
from django.contrib import messages
from django.shortcuts import reverse
# Create your views here.

class RealisateurListView(ListView):
    model = Realisateur

class RealisateurDetailView(DetailView):
    model = Realisateur

class RealisateurDeleteView(DeleteView):
    model = Realisateur
    template_name_suffix = '_delete'

    def get_success_url(self):
        messages.success(self.request, 'Realisateur Supprimé')
        return reverse('movies:realisateur_list')

class RealisateurUpdateView(UpdateView):
    model = Realisateur
    fields = ('nom','description','link_img')
    template_name_suffix = '_edit'

    def get_success_url(self):
        messages.success(self.request, 'Realisateur Mise à jour')
        return reverse('movies:realisateur_list')

class RealisateurCreateView(CreateView):
    model = Realisateur
    fields = ('nom','description', 'link_img')
    template_name_suffix = '_create'

    def get_success_url(self):
        messages.success(self.request, 'Realisateur Mise à jour')
        return reverse('movies:realisateur_list')