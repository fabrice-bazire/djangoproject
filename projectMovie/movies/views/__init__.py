from .viewFilm import FilmList, FilmDetailView, FilmCreate, FilmUpdate, FilmDelete
from .viewHome import home
from .viewSerie import SerieListView, SerieDetailView, SerieUpdateView, SerieCreateView, SerieDeleteView
from .viewReal import RealisateurListView, RealisateurDetailView, RealisateurDeleteView, RealisateurUpdateView, RealisateurCreateView
from .viewHome import home, connexion, showimage
