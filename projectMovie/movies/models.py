from django.db import models
from django.shortcuts import redirect
from datetime import datetime

# Create your models here.

class Image(models.Model):
    name= models.CharField(max_length=500)
    imagefile= models.FileField(upload_to='media/images/', null=True, verbose_name="")

    def __str__(self):
        return self.name 


class Realisateur (models.Model):
    nom = models.CharField(max_length=64)
    description = models.CharField(max_length=2500, default="no description") 
    link_img = models.CharField(max_length=350, null=True)

    def __str__ (self) :
        return self.nom

    @classmethod
    def getRealbyNom (cls, nom) : 
        return Realisateur.objects.get(nom = nom)


class Film (models.Model):
    nom = models.CharField(max_length=64)
    sortie = models.DateField(null=True, default=datetime.now())
    description = models.CharField(max_length=2500)
    realisateurs = models.ManyToManyField(Realisateur, related_name="films")
    img = models.ForeignKey(Image, on_delete=models.CASCADE, null=True)
    link_img = models.CharField(max_length=350, null=True)

    def __str__ (self) :
        return self.nom 

    def get_absolute_url(self): # new
        return redirect('http://localhost:8000/movies/films')


class Serie(models.Model):
    nom = models.CharField(max_length=64)
    sortie = models.DateField(null=True, default=datetime.now())
    description = models.CharField(max_length=2500)
    nb_saison = models.IntegerField()
    realisateurs = models.ManyToManyField(Realisateur, related_name="series")
    image = models.ForeignKey(Image, on_delete=models.CASCADE, null=True)
    link_img = models.CharField(max_length=350, null=True)
    def __str__ (self) :
        return self.nom

