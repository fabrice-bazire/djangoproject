from django.urls import reverse
from django.test import TestCase
from django.urls.exceptions import NoReverseMatch

# Create your tests here.

from movies.models import Serie,Realisateur, Film

class RealisateurTestCase(TestCase):
	def setUp(self):
		Realisateur.objects.create(nom='Bill', prenom="Murray", nationalite="americaine", alive=1)

	def test_realisateur_exist(self):
		rea = Realisateur.objects.get(pk=1)
		self.assertNotEqual(rea.nom, None)

	def test_real_url(self):
	    try:
	        url = reverse('movies:realisateur_detail', args=[1])
	    except:
	        assert False


class SerieTestCase(TestCase):
    def setUp(self):
        Serie.objects.create(nom='serie1', annee_sortie='2020-01-01', description="hello", nb_saison=2)
        Serie.objects.create(nom='serie2', annee_sortie='2020-01-01', description="hello", nb_saison=2)
        Serie.objects.create(nom='serie3', annee_sortie='2020-01-01', description="hello", nb_saison=2)
    
    def test_serie_url_name(self):
        try:
            url = reverse('movies:serie_detail', args=[1])
        except NoReverseMatch:
            assert False

    def test_serie_url(self):
        serie = Serie.objects.get(nom='serie1')
        url = reverse('movies:serie_edit',args=[serie.pk])
        response = self.client.get(url)
        assert response.status_code == 200

class FilmTestCase(TestCase):
    def setUp(self):
        Film.objects.create(nom='film1', annee_sortie='2020-01-01', description="hello")
        Film.objects.create(nom='film2', annee_sortie='2020-01-01', description="hello")
        Film.objects.create(nom='film3', annee_sortie='2020-01-01', description="hello")
    
    def test_film_url_name(self):
        try:
            url = reverse('movies:film_details', args=[1])
        except NoReverseMatch:
            assert False

    def test_film_url(self):
        films = Film.objects.get(nom='film1')
        url = reverse('movies:modif_film',args=[films.pk])
        response = self.client.get(url)
        assert response.status_code == 200