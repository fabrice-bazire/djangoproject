import scrapy
from urllib.parse import urljoin
from movies.models import Serie, Realisateur, Film
from scrap.items import FilmItem, RealisateurItem, FilmRealItem

class FilmSpider(scrapy.Spider):
    name = "film"
    custom_settings = {'ITEM_PIPELINES': {'scrap.pipelines.FilmRealPipeline': 400}}
    start_urls = [
         "https://www.senscritique.com/films/toujours-a-l-affiche",
         "https://www.senscritique.com/top/resultats/Les_meilleurs_films_de_2020/2582670"
    ]


    def parse(self,response):
        for sel in response.xpath('//div//@data-sc-link'):
            if "/film" in sel.get():
                base_url = "https://www.senscritique.com/"
                final_url = urljoin(base_url, sel.get())
                yield scrapy.Request(final_url, callback=self.parse_film)

    def parse_film(self, response):
        for data in response.xpath('//div[@class="wrap"]'):
            item = {}
            film = FilmRealItem()
            film['nom'] = "".join(data.xpath('//section/div/div/div/div[1]/h1//text()').extract())
            film['sortie'] = "".join(data.xpath('//li[@class="pvi-productDetails-item nowrap"]//time/@datetime').extract())
            film['description'] = "".join(data.xpath('//div[3]/div/section[1]/p/text()').extract())
            film['link_img'] = "".join(data.xpath('//div[@class="wrap__content"]/section/div/div/figure//a/@href').extract())
            item['film'] = film

            for sel in response.xpath('//div[2]/div[3]/div/section[1]/ul/li[1]/span//a//@href'):
                print(sel.get())

            for sel in response.xpath('//div[2]/div[3]/div/section[1]/ul/li[1]/span[@itemprop="director"]//a//@href'):
                base_url = "https://www.senscritique.com/"
                final_url = urljoin(base_url, sel.get())
                request = scrapy.Request(final_url, callback=self.parse_real)
                request.meta['item'] = item
                yield request

    def parse_real(self, response):
        print(response)
        for data in response.xpath('/html/body/div[1]/div[2]/section'):
            item = response.meta['item']
            real = RealisateurItem()
            real['nom'] = "".join(data.xpath('//div/div//h1//text()').extract()).strip()
            real['description'] = "".join(data.xpath('//div/div/p//text()').extract()).strip()
            real['link_img'] = "".join(data.xpath('//div/img/@src').extract()).strip()
            item['realisateurs'] = real
            yield item

            # Recherche seulement les oeuvres connus du realisateurs
        for sel in response.xpath('/html/body/div[1]/div[2]/div[3]/div/ul/li[@class="cvi-knowproducts-item"]/a/@href'):
            if "/film" in sel.get():
                base_url = "https://www.senscritique.com"
                final_url = urljoin(base_url, sel.get())
                yield scrapy.Request(final_url, callback=self.parse_film)
