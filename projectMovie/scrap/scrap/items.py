# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy_djangoitem import DjangoItem
from movies.models import Serie, Film, Realisateur

class SerieRealItem(DjangoItem):
    django_model = Serie

class FilmRealItem(DjangoItem):
    django_model = Film

class RealisateurItem(DjangoItem):
    # define the fields for your item here like:
    # name = scrapy.Field()
    django_model = Realisateur

class SerieItem(DjangoItem):
    # define the fields for your item here like:
    # name = scrapy.Field()
    django_model = Serie

class FilmItem(DjangoItem):
    # define the fields for your item here like:
    # name = scrapy.Field()
    django_model = Film

